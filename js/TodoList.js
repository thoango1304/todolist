export class TodoList {
    constructor() {
        this.tdList = [];
    }

    addTodo(todo) {
        this.tdList.push(todo);
    }

    removeTodo(index) {
        this.tdList.splice(index, 1);
    }

    renderTodo() {
        let content = "";
        content = this.tdList.reduce((todoContent, item, index) => {
            return todoContent += `
                <li>
                    <span>${item.text}</span>
                    <div class="buttons">
                        <button class="remove" data-index="${index}" data-status="${item.status}" onclick="deleteTodo(event)">
                            <i class="fa fa-trash-alt"></i>
                        </button>
                        <button class="complete" data-index="${index}" data-status="${item.status}" onclick="completeTodo(event)">
                            <i class="fas fa-check-circle"></i>
                            <i class="far fa-check-circle"></i>
                        </button>
                    </div>
                </li>
            `;
        }, "");

        return content;
    }

    sortTodo(isDES) {
        this.tdList.sort((todo, nextTodo) => {
            let textA = todo.text.toLowerCase();
            let textB = nextTodo.text.toLowerCase();

            return textA.localeCompare(textB);
        })

        if (isDES) {
            this.tdList.reverse();
        }
        
    }
}