import { Todo } from "./Todo.js";
import { TodoList } from "./TodoList.js";

let todoList = new TodoList();

let completedList = new TodoList();

const getELE = (id) => {
    return document.getElementById(id);
}

const themTodo = () => {
    let textTodo = getELE("newTask").value;

    if (textTodo != "") {
        let todo = new Todo(textTodo, "todo");
        todoList.addTodo(todo);
    }

    showTodo();
    getELE("newTask").value = "";
}

getELE("addItem").addEventListener("click", () => {
    themTodo();
});

const showTodo = () => {
    let ulTodo = getELE("todo");
    ulTodo.innerHTML = todoList.renderTodo();
}

const showCompletedTodo = () => {
    let ulCompleted = getELE("completed");
    ulCompleted.innerHTML = completedList.renderTodo();
}

const deleteTodo = (e) => {
    let indexTodo = e.currentTarget.getAttribute("data-index");
    let status = e.currentTarget.getAttribute("data-status");

    if (status == "todo") {
        todoList.removeTodo(indexTodo);
        showTodo();
    } else if (status == "completed") {
        completedList.removeTodo(indexTodo);
        showCompletedTodo();
    } else {
        alert("Cannot delete todo!");
    }
}

window.deleteTodo = deleteTodo;

const completeTodo = (e) => {
    let indexTodo = e.currentTarget.getAttribute("data-index");
    let status = e.currentTarget.getAttribute("data-status");

    if (status == "todo") {
        let completedTodo = new Todo(todoList.tdList[indexTodo].text, "completed");
        
        completedList.addTodo(completedTodo);
        todoList.removeTodo(indexTodo);

        showCompletedTodo();
        showTodo();
    } else if (status == "completed") {
        let undoTodo = new Todo(completedList.tdList[indexTodo].text, "todo");

        todoList.addTodo(undoTodo);
        completedList.removeTodo(indexTodo);

        showCompletedTodo();
        showTodo();
    } else {
        alert("Cannot complete todo!");
    }
}

window.completeTodo = completeTodo;

const sortASC = () => {
    todoList.sortTodo(false);
    showTodo();
}
window.sortASC = sortASC;

const sortDES = () => {
    todoList.sortTodo(true);
    showTodo();
}

window.sortDES = sortDES;